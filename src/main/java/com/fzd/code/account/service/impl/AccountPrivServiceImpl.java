package com.fzd.code.account.service.impl;

import com.fzd.code.account.orm.model.AccountPriv;
import com.fzd.code.account.orm.mapper.AccountPrivMapper;
import com.fzd.code.account.service.IAccountPrivService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2018-07-17
 */
@Service
public class AccountPrivServiceImpl extends ServiceImpl<AccountPrivMapper, AccountPriv> implements IAccountPrivService {

}
