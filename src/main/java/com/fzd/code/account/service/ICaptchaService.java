package com.fzd.code.account.service;

import com.fzd.code.account.orm.model.Account;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface ICaptchaService {
    void create(String id, HttpServletResponse response) throws Exception;
    boolean valid(String id,String code);
}
