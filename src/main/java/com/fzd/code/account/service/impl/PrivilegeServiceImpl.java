package com.fzd.code.account.service.impl;

import com.fzd.code.account.orm.model.Privilege;
import com.fzd.code.account.orm.mapper.PrivilegeMapper;
import com.fzd.code.account.service.IPrivilegeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2018-07-17
 */
@Service
public class PrivilegeServiceImpl extends ServiceImpl<PrivilegeMapper, Privilege> implements IPrivilegeService {

}
