package com.fzd.code.account.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fzd.code.account.orm.mapper.AccountMapper;
import com.fzd.code.account.orm.model.Account;
import com.fzd.code.account.service.IAccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2018-07-17
 */
@Service
@Slf4j
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements IAccountService {

}
