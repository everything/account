package com.fzd.code.account.service.impl;

import com.fzd.code.account.orm.model.PrivMenu;
import com.fzd.code.account.orm.mapper.PrivMenuMapper;
import com.fzd.code.account.service.IPrivMenuService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2018-07-17
 */
@Service
public class PrivMenuServiceImpl extends ServiceImpl<PrivMenuMapper, PrivMenu> implements IPrivMenuService {

}
