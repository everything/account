package com.fzd.code.account.service.impl;

import com.fzd.code.account.orm.model.Plat;
import com.fzd.code.account.orm.mapper.PlatMapper;
import com.fzd.code.account.service.IPlatService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2018-07-17
 */
@Service
public class PlatServiceImpl extends ServiceImpl<PlatMapper, Plat> implements IPlatService {

}
