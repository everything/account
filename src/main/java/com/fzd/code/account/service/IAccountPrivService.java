package com.fzd.code.account.service;

import com.fzd.code.account.orm.model.AccountPriv;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2018-07-17
 */
public interface IAccountPrivService extends IService<AccountPriv> {

}
