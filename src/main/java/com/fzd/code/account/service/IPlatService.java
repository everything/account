package com.fzd.code.account.service;

import com.fzd.code.account.orm.model.Plat;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2018-07-17
 */
public interface IPlatService extends IService<Plat> {

}
