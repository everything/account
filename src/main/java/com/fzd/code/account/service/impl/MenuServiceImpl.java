package com.fzd.code.account.service.impl;

import com.fzd.code.account.orm.model.Menu;
import com.fzd.code.account.orm.mapper.MenuMapper;
import com.fzd.code.account.service.IMenuService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2018-07-17
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {

    @Autowired
    private MenuMapper menuMapper;

    @Override
    public List<Menu> selectAccountMenu(String accountId) {
        return this.menuMapper.selectAccountMenu(accountId);
    }
}
