package com.fzd.code.account.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.fzd.code.account.orm.model.Account;
import com.fzd.code.account.service.IAccountService;
import com.fzd.code.account.service.IAuthService;
import com.fzd.code.encrpty.EncrptyUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("common-auth-service")
@Slf4j
public class CommonAuthServiceImpl implements IAuthService {

    @Autowired
    private IAccountService accountService;

    @Override
    public Account login(Account account) throws Exception{
        Account loginWhere=new Account();
        loginWhere.setAccountId(account.getAccountId());
        loginWhere.setPassword(EncrptyUtil.md5(account.getPassword()));
        Account loggedAccount=this.accountService.selectOne(new EntityWrapper<>(loginWhere));
        if (loggedAccount==null) {
            return null;
        }
        return loggedAccount;
    }

    @Override
    public void logout(String accountId) {

    }
}
