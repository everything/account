package com.fzd.code.account.service;

import com.fzd.code.account.orm.model.Menu;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2018-07-17
 */
public interface IMenuService extends IService<Menu> {
    List<Menu> selectAccountMenu(String accountId);
}
