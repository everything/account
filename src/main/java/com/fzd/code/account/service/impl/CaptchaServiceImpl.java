package com.fzd.code.account.service.impl;

import com.fzd.code.account.ServiceConfig;
import com.fzd.code.account.service.ICaptchaService;
import com.fzd.code.captcha.Captcha;
import com.fzd.code.captcha.service.i.IRedisCaptchaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;

@Service
public class CaptchaServiceImpl implements ICaptchaService {

    @Autowired
    private ServiceConfig serviceConfig;
    @Qualifier("redis-mycatchap-service")
    @Autowired
    private IRedisCaptchaService myCaptchaService;
    @Qualifier("redis-jcatchap-service")
    @Autowired
    private IRedisCaptchaService jCaptchaService;

    @Override
    public void create(String id, HttpServletResponse response) throws Exception {
        getCaptchaService().create(id,response);
    }

    @Override
    public boolean valid(String id,String code) {
        return getCaptchaService().validate(id,code);
    }

    private IRedisCaptchaService getCaptchaService(){
        switch (Captcha.valueOf(serviceConfig.getLoginCaptcha())){
            case JCaptcha:
                return jCaptchaService;
            case MyCaptcha:
                return myCaptchaService;
        }
        return null;
    }
}
