package com.fzd.code.account.service;

import com.fzd.code.account.orm.model.Account;

public interface IAuthService {
    Account login(Account account) throws Exception;
    void logout(String accountId) throws Exception;
}
