package com.fzd.code.account.service;

import com.baomidou.mybatisplus.service.IService;
import com.fzd.code.account.orm.model.Account;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2018-07-17
 */
public interface IAccountService extends IService<Account> {
}
