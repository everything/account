package com.fzd.code.account;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.config.rules.DbType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

public class MyBatisPlusGeneratorMysql {

	public static void main(String[] args) {

		AutoGenerator mpg = new AutoGenerator();

		// 全局配置
		GlobalConfig gc = new GlobalConfig();
		gc.setOutputDir("D:\\code-account-mybatis-plus-code");// 这里写你自己的java目录
		gc.setFileOverride(true);// 是否覆盖
		gc.setActiveRecord(true);
		gc.setEnableCache(false);// XML 二级缓存
		gc.setBaseResultMap(true);// XML ResultMap
		gc.setBaseColumnList(false);// XML columList
		mpg.setGlobalConfig(gc);

		// 数据源配置
		DataSourceConfig dsc = new DataSourceConfig();
		dsc.setDbType(DbType.MYSQL);
		dsc.setTypeConvert(new MySqlTypeConvert() {
			// 自定义数据库表字段类型转换【可选】
			@Override
			public DbColumnType processTypeConvert(String fieldType) {
				return super.processTypeConvert(fieldType);
			}
		});
		dsc.setDriverName("com.mysql.jdbc.Driver");
		dsc.setUsername("root");
		dsc.setPassword("mysql_fzd_abc_123");
		dsc.setUrl("jdbc:mysql://127.0.0.1:3306/account?useUnicode=true&amp;characterEncoding=UTF-8&amp;allowMultiQueries=true");
		mpg.setDataSource(dsc);

		// 策略配置
		StrategyConfig strategy = new StrategyConfig();
		strategy.setNaming(NamingStrategy.underline_to_camel);// 表名生成策略
		strategy.setEntityLombokModel(true);
		strategy.setInclude(include);
		mpg.setStrategy(strategy);

		// 包配置
		PackageConfig pc = new PackageConfig();
		pc.setParent(null);
		pc.setEntity("com.fzd.code.account.orm.model");
		pc.setMapper("com.fzd.code.account.orm.mapper");
		pc.setXml("com.fzd.code.account.orm.mapper.xml");
		pc.setService("com.fzd.code.account.service");
		pc.setServiceImpl("com.fzd.code.account.service.impl");
		pc.setController("com.fzd.code.account.web");
		mpg.setPackageInfo(pc);

		// 执行生成
		mpg.execute();
	}

	private static String[] include = new String[] {
			"plat","account","account_priv","menu","priv_menu","privilege"
	};

}