package com.fzd.code.account;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
public class LdapConfig extends com.fzd.code.ldap.LdapConfig {
	@Value("${ldap.domain}")
	private String domain;
	@Value("${ldap.isSsl}")
	private String isSsl;
	@Value("${ldap.address}")
	private String address;
	@Value("${ldap.portNormal}")
	private String portNormal;
	@Value("${ldap.portSsl}")
	private String portSsl;
}
