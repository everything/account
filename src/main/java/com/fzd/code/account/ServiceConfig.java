package com.fzd.code.account;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Getter
@Component
public class ServiceConfig {
    @Value("${login.captcha}")
    private String loginCaptcha;
}
