package com.fzd.code.account.web;


import com.fzd.code.account.orm.model.Plat;
import com.fzd.code.account.service.IPlatService;
import com.fzd.code.auth.token.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2018-07-17
 */
@RestController
@RequestMapping("/plat")
public class PlatController {
    @Autowired
    private IPlatService platService;

    @PutMapping
    @Authorization
    public boolean update(Plat plat){
        return this.platService.updateById(plat);
    }

    @GetMapping
    public Plat get(){
        return this.platService.selectList(null).get(0);
    }
}

