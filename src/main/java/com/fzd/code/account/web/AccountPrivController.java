package com.fzd.code.account.web;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2018-07-17
 */
@Controller
@RequestMapping("/accountPriv")
public class AccountPrivController {

}

