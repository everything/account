package com.fzd.code.account.web;


import com.fzd.code.account.orm.model.Menu;
import com.fzd.code.account.service.IMenuService;
import com.fzd.code.auth.token.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2018-07-17
 */
@RestController
@RequestMapping("/menu")
public class MenuController {

    @Autowired
    private IMenuService menuService;

    @GetMapping("/account/{accountId}")
    @Authorization
    public List<Menu> getAccountMenu(@PathVariable String accountId){
        return this.menuService.selectAccountMenu(accountId);
    }

}

