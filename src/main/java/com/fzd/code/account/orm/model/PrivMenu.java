package com.fzd.code.account.orm.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author ${author}
 * @since 2018-07-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("priv_menu")
public class PrivMenu extends Model<PrivMenu> {

    private static final long serialVersionUID = 1L;

	@TableField("priv_name")
	private String privName;
	@TableField("menu_id")
	private Integer menuId;
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;


	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
