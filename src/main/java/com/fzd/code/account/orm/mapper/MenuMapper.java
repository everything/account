package com.fzd.code.account.orm.mapper;

import com.fzd.code.account.orm.model.Menu;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2018-07-17
 */
public interface MenuMapper extends BaseMapper<Menu> {
    List<Menu> selectAccountMenu(@Param("accountId") String accountId);
}
