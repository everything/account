package com.fzd.code.account.orm.mapper;

import com.fzd.code.account.orm.model.Account;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2018-07-17
 */
public interface AccountMapper extends BaseMapper<Account> {

}
