package com.fzd.code.account.orm.mapper;

import com.fzd.code.account.orm.model.PrivMenu;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2018-07-17
 */
public interface PrivMenuMapper extends BaseMapper<PrivMenu> {

}
