package com.fzd.code.account.orm.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author ${author}
 * @since 2018-07-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Account extends Model<Account> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
	@TableField("account_id")
	private String accountId;
	private String password;
	@TableField("mobile_no")
	private String mobileNo;
	@TableField("user_name")
	private String userName;
	private String department;

	@TableField(exist = false)
	private String captchaId;
	@TableField(exist = false)
	private String captcha;

	private String source;

	@TableField(exist = false)
	private String loginMethod; //登录方式 普通登录|ldap登录等

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
