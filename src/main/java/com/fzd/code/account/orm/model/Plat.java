package com.fzd.code.account.orm.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author ${author}
 * @since 2018-07-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Plat extends Model<Plat> {

    private static final long serialVersionUID = 1L;

	private String name;
	private String project;
	private String version;
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;


	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
